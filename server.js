const express = require('express');
const app = express();
const PORT = process.env.PORT || 3001;
var server = app.listen(PORT, () => {
	console.log('server is connected on :' + PORT);
});

app.get('/', (req, res) => {
	res.render('index');
});

//app config
app.set('view engine', 'ejs');

app.use('/assets', express.static('views/assets'));
